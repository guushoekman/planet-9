$(".board svg path").click(function() {
	$(".column.info.global").removeClass("visible");
	$(".column.info.single-tile").addClass("visible");

	var tileCard = $(".single-tile.card");
	var type = $(this).data("type");
	var owner = $(this).data("owner");
	var impact = $(this).data("impact");
	var id = $(this).attr("id");

	tileCard.find("img").attr("src", "img/" + type + ".jpg").attr("alt", type);
	tileCard.find(".tile-type").text(type);
	tileCard.find(".tile-owner").text(owner);
	tileCard.find(".tile-impact").text(impact);
	tileCard.find(".tile-id").text(id);
})

$(".button.to-board").click(function() {
	$(".column.info.single-tile").removeClass("visible");
	$(".column.info.global").addClass("visible");
})